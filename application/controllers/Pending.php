<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pending extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ModelJemaat', 'jemaat');
    }

    public function index()
    {
        $data['pending'] = $this->db->query("SELECT * from jemaat J, sektor S, user U WHERE S.id_sektor=J.sektor AND statuspost='0' AND U.id_user=J.id_pengirim")->result();
        $data['jemaat'] = $this->jemaat->lihatData();

		$this->load->view('pending/index', $data);
    }

    public function setuju()
    {
        $id = $this->input->post('id_jemaat');
        $statuspost = '1';

        $data = array('statuspost' => $statuspost);
        $where = array('id_jemaat' => $id);

        $this->jemaat->setuju($where, $data, 'jemaat');
        redirect('pending/index');

    }
}

?>