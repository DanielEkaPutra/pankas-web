<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jemaat extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ModelJemaat', 'modeljemaat');
    }

    public function index()
    {
        
        $this->load->library('pagination');
        
        //ambil data keyword

        if($this->input->post('submit')){
        
            $data['keyword'] = $this->input->post('keyword');
            $this->session->set_userdata('keyword', $data['keyword']);
        } else {
            $data['keyword'] = $this->session->userdata('keyword');
        }

        $config['base_url'] = 'http://localhost/test-login/jemaat/index/';
        $this->db->like('nama', $data['keyword']);
        $config['total_rows'] = $this->modeljemaat->total_rows();
        $data['total_rows'] = $config['total_rows'];
        $config['per_page'] = 10;
        
        
        $config['full_tag_open'] = '<nav> <ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        
        
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="page-item">';   
        $config['first_tag_close'] = '</li>';   
        
        
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="page-item">';   
        $config['last_tag_close'] = '</li>';   
        
        
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li class="page-item">';   
        $config['next_tag_close'] = '</li>'; 
        

        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="page-item">';   
        $config['prev_tag_close'] = '</li>'; 
        
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';   
        $config['cur_tag_close'] = '</a></li>'; 
        
        $config['num_tag_open'] = '<li class="page-item">';   
        $config['num_tag_close'] = '</li>'; 

        $config['attributes'] = array('class' => 'page-link');
        
        $this->pagination->initialize($config);

        $where = array('statuspost' =>'1');

        $data['start']  = $this->uri->segment(3);
        $data['jemaat'] = $this->modeljemaat->lihatData1($config['per_page'], $data['start'], $data['keyword'], $where);
        $data['jemaat1'] = $this->modeljemaat->lihatData();

        $this->load->view('lihatdata/index', $data);
     
    }

    public function tambah_data()
    {
        $id_jemaat = $this->input->post('id_jemaat');
        $nama = $this->input->post('nama');
        $email = $this->input->post('email');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $tempat_lahir = $this->input->post('tempat_lahir');
        $tgl_lahir = $this->input->post('tgl_lahir');
        $tempat_baptis = $this->input->post('tempat_baptis');
        $tgl_baptis = $this->input->post('tgl_baptis');
        $tempat_sidi = $this->input->post('tempat_sidi');
        $tgl_sidi = $this->input->post('tgl_sidi');
        $tempat_nikah = $this->input->post('tempat_nikah');
        $tgl_nikah = $this->input->post('tgl_nikah');
        $tempat_pindah = $this->input->post('tempat_pindah');
        $tgl_pindah = $this->input->post('tgl_pindah');
        $tempat_meninggal = $this->input->post('tempat_meninggal');
        $tgl_meninggal = $this->input->post('tgl_meninggal');
        $pendidikan = $this->input->post('pendidikan');
        $sektor = $this->input->post('sektor');
        $telepon = $this->input->post('telepon');
        $pekerjaan = $this->input->post('pekerjaan');
        $alamat = $this->input->post('alamat');
        $kota = $this->input->post('kota');
        $provinsi = $this->input->post('provinsi');
        $id_pengirim = $this->input->post('id_pengirim');

        $data = array(
            'id_jemaat' => $id_jemaat,
            'nama' => $nama,
            'email' => $email,
            'jenis_kelamin' => $jenis_kelamin,
            'tempat_lahir' => $tempat_lahir,
            'tgl_lahir' => $tgl_lahir,
            'tempat_baptis' => $tempat_baptis,
            'tgl_baptis' => $tgl_baptis,
            'tempat_sidi' => $tempat_sidi,
            'tgl_sidi' => $tgl_sidi,
            'tempat_nikah' => $tempat_nikah,
            'tgl_nikah' => $tgl_nikah,
            'tempat_pindah' => $tempat_pindah,
            'tgl_pindah' => $tgl_pindah,
            'tempat_meninggal' => $tempat_meninggal,
            'tgl_meninggal' => $tgl_meninggal,
            'pendidikan' => $pendidikan,
            'sektor' => $sektor,
            'telepon' => $telepon,
            'pekerjaan' => $pekerjaan,
            'alamat' => $alamat,
            'kota' => $kota,
            'provinsi' => $provinsi,
            'id_pengirim' => $id_pengirim
        );

        if($data > 0)
        {
            $this->modeljemaat->tambahData($data);
            redirect('jemaat/index');
        }

    }

    public function edit_data($id)
    {
        $where = array('id_jemaat' =>$id);
        $data['jemaat'] = $this->modeljemaat->editData($where, 'jemaat')->result();

		$this->load->view('editdata/index', $data);
    }

    public function getupdate_data()
    {
        echo json_encode($this->modeljemaat->lihatDataById($_POST['id']));
    }
    public function update_data()
    {
        $id = $this->input->post('id_jemaat');
        $nama = $this->input->post('nama');
        $email = $this->input->post('email');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $tempat_lahir = $this->input->post('tempat_lahir');
        $tgl_lahir = $this->input->post('tgl_lahir');
        $tempat_baptis = $this->input->post('tempat_baptis');
        $tgl_baptis = $this->input->post('tgl_baptis');
        $tempat_sidi = $this->input->post('tempat_sidi');
        $tgl_sidi = $this->input->post('tgl_sidi');
        $tempat_nikah = $this->input->post('tempat_nikah');
        $tgl_nikah = $this->input->post('tgl_nikah');
        $tempat_pindah = $this->input->post('tempat_pindah');
        $tgl_pindah = $this->input->post('tgl_pindah');
        $tempat_meninggal = $this->input->post('tempat_meninggal');
        $tgl_meninggal = $this->input->post('tgl_meninggal');
        $pendidikan = $this->input->post('pendidikan');
        $sektor = $this->input->post('sektor');
        $telepon = $this->input->post('telepon');
        $pekerjaan = $this->input->post('pekerjaan');
        $alamat = $this->input->post('alamat');
        $kota = $this->input->post('kota');
        $provinsi = $this->input->post('provinsi');
        $id_pengirim = $this->input->post('id_pengirim');
        $status_post = '0';
        
        $data = array(
            'nama' => $nama,
            'email' => $email,
            'jenis_kelamin' => $jenis_kelamin,
            'tempat_lahir' => $tempat_lahir,
            'tgl_lahir' => $tgl_lahir,
            'tempat_baptis' => $tempat_baptis,
            'tgl_baptis' => $tgl_baptis,
            'tempat_sidi' => $tempat_sidi,
            'tgl_sidi' => $tgl_sidi,
            'tempat_nikah' => $tempat_nikah,
            'tgl_nikah' => $tgl_nikah,
            'tempat_pindah' => $tempat_pindah,
            'tgl_pindah' => $tgl_pindah,
            'tempat_meninggal' => $tempat_meninggal,
            'tgl_meninggal' => $tgl_meninggal,
            'pendidikan' => $pendidikan,
            'sektor' => $sektor,
            'telepon' => $telepon,
            'pekerjaan' => $pekerjaan,
            'alamat' => $alamat,
            'kota' => $kota,
            'provinsi' => $provinsi,
            'id_pengirim' => $id_pengirim,
            'statuspost' => $status_post
        );


        $where = array(
            'id_jemaat' => $id
        );

        $this->modeljemaat->updateData('jemaat', $data, $where);
        redirect('jemaat/index');
       
    }



    public function hapus_data($id)
    {
        $where = array('id_jemaat' => $id);
        $this->modeljemaat->hapusData($where, 'jemaat');
        redirect('jemaat/index');
    }

    public function testing()
    {
        $data['jemaat'] = $this->modeljemaat->lihatDataById(101020);
        $this->load->view('test-view', $data);
    }
}


?>