<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
 {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('ModelUser', 'modeluser');
    }

	public function index()
	{
		$this->load->view('templates/auth_header');
		$this->load->view('auth/login');
		$this->load->view('templates/auth_footer');
	}

	public function registration()
	{
		$this->load->view('templates/auth_header');
		$this->load->view('auth/registration');
		$this->load->view('templates/auth_footer');
	}

	public function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$ceklogin = $this->modeluser->login($username, $password);
		if($ceklogin)
		{
			foreach($ceklogin as $row);
			$sessionArr = array(
				'id_user' => $row["id_user"],
				'hak_akses' => $row["hak_akses"],
				'username' => $row["username"],
				'password' => $row["password"],
				'nama_lengkap' => $row["nama_lengkap"]
			);
			$this->session->set_userdata($sessionArr);
          	redirect('menu', 'refresh');
		}else{
			$this->session->unset_userdata('username');
            $this->session->unset_userdata('password');
            $this->session->unset_userdata('hak_akses');
			$this->session->unset_userdata('nama_lengkap');
			$this->session->set_flashdata('login', 'failed');
			redirect('auth', 'refresh');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('auth');
	}

}