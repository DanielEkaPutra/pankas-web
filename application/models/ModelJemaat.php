<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelJemaat extends CI_Model
{

    /*protected $table = 'jemaat';
    protected $useTimestamps = true;*/

    // public function getAllJemaat()
    // {
    //     $this->db->query('SELECT * FROM ' . $this->table);
    //     return $this->db->resultSet();
    // }
    
    public function getPending($table)
    {
        return $this->db->get_where($table, array('statuspost' => '0'));
    }

    public function tambahData($data)
    {
        $this->db->insert('jemaat', $data);
    }

    public function editData($where, $table)
    {
        return $this->db->get_where($table, $where);
        return $this->db->from('jemaat')
            ->join('sektor', 'sektor.id_sektor=jemaat.sektor')
            ->get()
            ->result();
    }

    public function updateData($table,$data,$where)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function hapusData($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    public function total_rows()
    {
        return $this->db->get_where('jemaat', array('statuspost' => '1'))->num_rows();
    }
    
    public function setuju($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function lihatData()
    {
        $this->db->select('*');
        $this->db->from('jemaat');
        return $this->db->get()->result();
    }

    public function lihatDataById($id)
    {
        $this->db->select('*');
        $this->db->from('jemaat');
        $this->db->where('id_jemaat' , $id);
        return $this->db->get()->result();
    }

    public function lihatData1($limit, $start, $keyword , $where)
    {
        if($keyword) {
            $this->db->like('nama', $keyword);
            
        }
        $this->db->select('*');
        $this->db->from('jemaat');
        $this->db->join('sektor', 'sektor.id_sektor=jemaat.sektor');
        $this->db->where($where);
        $this->db->limit($limit,$start);
        $this->db->order_by('id_jemaat', 'ASC');
        
        return $this->db->get()->result();
        
    }

    public function hitungJemaat()
    {
        return $this->db->get('jemaat')->num_rows();
    }

    public function keywordjemaat()
    {
        $this->db->select('*');
        $this->db->from('jemaat');
        $this->db->like('nama', $keyword);
        return $this->db->get()->result();
    }

    /*public function hitungjemaat(){

        protected $table = 'jemaaat';      
    }*/
    /*protected $table = 'jemaat';
    protected $useTimestamps = 'true';*/
   /* protected $allowedFields = [];*/

}

?>