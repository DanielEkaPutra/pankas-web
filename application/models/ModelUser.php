<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ModelUser extends CI_Model
{
    public function login($username, $password)
    {
        $this->db->select('id_user, username, password, hak_akses, nama_lengkap');
        $this->db->from('user');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
       
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }else{
            return false;
        }
    }
}

?>