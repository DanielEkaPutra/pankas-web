
  <section class="counts" style="padding-top: 0;">
  <div class="container">
    <?php 
        foreach($pending as $p) : ?>
      <div class="row">
        <div class=" col-lg-12 col-md-12" style="width:100%; display: inline-block;">
          <div class="count-box">
            <div class="row">
              <div class="col-8" style="align-self: center;">
                <a style="text-decoration:none" role="button" data-toggle="modal" data-target="#pendingModal<?= $p->id_jemaat?>"><h6><b><?= $p->nama?></b></h6></a>
                <p> Sektor : <?= $p->sektor ?> </p>
              </div>
              <br>
              <div class="col-4" style="align-self: center; text-align:right;">
              </div>
            </div>
          </div>
        </div>
      </div>
            <?php endforeach; ?>
  </div>
</div>
        <!-- /.container-fluid -->
</div>
      <!-- End of Main Content -->

<script type="text/javascript">

$(document).ready( function () {
    $('#table').DataTable();
} );

</script>

</section>