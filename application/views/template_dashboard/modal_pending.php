<!-- Modal -->

<?php $no = 0;
foreach($pending as $p) : $no++;?>
<div class="modal fade" id="pendingModal<?= $p->id_jemaat?>" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">Edit Jemaat</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <form action="<?= site_url('pending/setuju');?>" method="post">
		<div class="row">
				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-3">
					<label for="IDJemaat">No ID Jemaat</label>
						<input type="number" readonly="" class="form-control" value="<?= $p->id_jemaat?>" id="IDJemaat" aria-describedby="id_jemaat" placeholder="" name="id_jemaat">
				</div>
				<div class="form-group col-3">
					<label for="Nama">Nama</label>
						<input type="text" class="form-control" value="<?= $p->nama?>" id="Nama" aria-describedby="Nama" placeholder="" name="nama">
				</div>
				
				<div class="form-group col-3">
					<label for="Email">Email</label>
						<input type="email" class="form-control" value="<?= $p->email?>" id="Alamat" aria-describedby="Alamat" placeholder="" name="email">
				</div>

				<div class="form-group col-3">
					<label for="JenisKelamin">Jenis Kelamin</label>
						<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
						<option <?php if($p->jenis_kelamin == "LAKI LAKI"){echo "selected='selected'";} echo $p->jenis_kelamin;?> value="Laki-Laki">Laki-Laki</option>
							<option <?php if($p->jenis_kelamin == "PEREMPUAN"){echo "selected='selected'";} echo $p->jenis_kelamin;?>value="Perempuan">Perempuan</option>
						</select>
				</div>
				
				</div>
				</div>
				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-4">
					<label for="Tempat_Lahir">Tempat Lahir</label>
						<input type="text" class="form-control" value="<?= $p->tempat_lahir?>" id="Tempat_Lahir" aria-describedby="Tempat_Lahir" placeholder="" name="tempat_lahir">
				</div>
				
				<div class="form-group col-4">
					<label for="Tempat_Baptis">Tempat Baptis</label>
						<input type="text" class="form-control" value="<?= $p->tempat_baptis?>" id="Tempat_Lahir" aria-describedby="Tempat_Lahir" placeholder="" name="tempat_baptis">
				</div>

				<div class="form-group col-4">
					<label for="Tempat_Sidi">Tempat Sidi</label>
						<input type="text" class="form-control" value="<?= $p->tempat_sidi?>" id="Tempat_Lahir" aria-describedby="Tempat_Lahir" placeholder="" name="tempat_sidi">
				</div>
				
				</div>
				</div>
				
				<div class="col-xl-12">
				<div class="row">
				
				<div class="form-group col-4">
				<label for="tgl_lahir">Tanggal Lahir</label>
						<input type="date" class="form-control" value="<?= $p->tgl_lahir?>" id="tgl_lahir" aria-describedby="tgl_lahir" placeholder="" name="tgl_lahir">
				</div>

				<div class="form-group col-4">
					<label for="tgl_lahir">Tanggal Baptis</label>
						<input type="date" class="form-control" value="<?= $p->tgl_baptis?>" id="tgl_lahir" aria-describedby="tgl_lahir" placeholder="" name="tgl_baptis">
				</div>
				<div class="form-group col-4">
					<label for="tgl_sidi">Tanggal Sidi</label>
						<input type="date" class="form-control" value="<?= $p->tgl_sidi?>" id="tgl_sidi" aria-describedby="tgl_sidi" placeholder="" name="tgl_sidi">
				</div>
				
                
				</div>
				</div>

				<div class="col-xl-12">
				<div class="row">
				
				<div class="form-group col-4">
					<label for="Tempat_Lahir">Tempat Nikah</label>
						<input type="text" class="form-control" value="<?= $p->tempat_nikah?>" id="Tempat_Lahir" aria-describedby="Tempat_Lahir" placeholder="" name="tempat_nikah">
				</div>

				<div class="form-group col-4">
					<label for="Tempat_Lahir">Tempat Pindah</label>
						<input type="text" class="form-control" value="<?= $p->tempat_pindah?>" id="Tempat_Lahir" aria-describedby="Tempat_Lahir" placeholder="" name="tempat_pindah">
				</div>
				<div class="form-group col-4">
					<label for="Tempat_Lahir">Tempat Meninggal</label>
						<input type="text" class="form-control" value="<?= $p->tempat_meninggal?>" id="Tempat_Lahir" aria-describedby="Tempat_Lahir" placeholder="" name="tempat_meninggal">
				</div>
				
                
				</div>
				</div>

				<div class="col-xl-12">
				<div class="row">
				
				<div class="form-group col-4">
					<label for="tgl_lahir">Tanggal Nikah</label>
						<input type="date" class="form-control" value="<?= $p->tgl_nikah?>" id="tgl_lahir" aria-describedby="tgl_lahir" placeholder="" name="tgl_nikah">
				</div>

				<div class="form-group col-4">
					<label for="tgl_lahir">Tanggal Pindah</label>
						<input type="date" class="form-control" value="<?= $p->tgl_pindah?>" id="tgl_lahir" aria-describedby="tgl_lahir" placeholder="" name="tgl_pindah">
				</div>
				<div class="form-group col-4">
					<label for="tgl_sidi">Tanggal Meninggal</label>
						<input type="date" class="form-control" value="<?= $p->tgl_meninggal?>" id="tgl_sidi" aria-describedby="tgl_sidi" placeholder="" name="tgl_meninggal">
				</div>
				
                
				</div>
				</div>

				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-3">
					<label for="pendidikan">Pendidikan</label>
						<input type="text" class="form-control" value="<?= $p->pendidikan?>" id="pendidikan" aria-describedby="pendidikan" placeholder="" name="pendidikan">
				</div>
				<div class="form-group col-3">
					<label for="IDSektor">Sektor</label>
					<select name="sektor" id="sektor" class="form-control">
					<option value="0">Pilih Salah Satu</option>
						<option <?php if($p->sektor == "1"){echo "selected='selected'";} echo $p->sektor;?> value="1">Yerusalem</option>
						<option <?php if($p->sektor == "2"){echo "selected='selected'";} echo $p->sektor;?> value="2">Betel</option>
						<option <?php if($p->sektor == "3"){echo "selected='selected'";} echo $p->sektor;?> value="3">Sion</option>
						<option <?php if($p->sektor == "4"){echo "selected='selected'";} echo $p->sektor;?> value="4">Hermon</option>
						<option <?php if($p->sektor == "5"){echo "selected='selected'";} echo $p->sektor;?> value="5">Yerikho</option>
						<option <?php if($p->sektor == "6"){echo "selected='selected'";} echo $p->sektor;?> value="6">Karmel</option>
						<option <?php if($p->sektor == "7"){echo "selected='selected'";} echo $p->sektor;?> value="7">Pniel</option>
						<option <?php if($p->sektor == "8"){echo "selected='selected'";} echo $p->sektor;?> value="8">Nazaret</option>
						<option <?php if($p->sektor == "9"){echo "selected='selected'";} echo $p->sektor;?> value="9">Moria</option>
						<option <?php if($p->sektor == "10"){echo "selected='selected'";} echo $p->sektor;?> value="10">Getsemani</option>
						<option <?php if($p->sektor == "11"){echo "selected='selected'";} echo $p->sektor;?> value="11">Betesda</option>
						<option <?php if($p->sektor == "12"){echo "selected='selected'";} echo $p->sektor;?> value="12">Betlehem</option>
						<option <?php if($p->sektor == "13"){echo "selected='selected'";} echo $p->sektor;?> value="13">Betesda</option>
					</select>
				</div>
				
				<div class="form-group col-3">
					<label for="Telepon">Telepon</label>
						<input type="number" class="form-control" value="<?= $p->telepon?>" id="Telepon" aria-describedby="Telepon" placeholder="" name="telepon">
				</div>
				
				<div class="form-group col-3">
					<label for="pekerjaan">Pekerjaan</label>
						<input type="text" class="form-control" value="<?= $p->pekerjaan?>" id="pekerjaan" aria-describedby="pekerjaan" placeholder="" name="pekerjaan">
				</div>

				</div>
				</div>

				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-3">
					<label for="hub_keluarga">Alamat</label>
						<input type="text" class="form-control" value="<?= $p->alamat?>" id="hub_keluarga" aria-describedby="hub_keluarga" placeholder="" name="alamat">
				</div>				
				
				<div class="form-group col-3">
					<label for="gol_darah">Kota</label>
						<input type="text" class="form-control" value="<?= $p->kota?>" id="gol_darah" aria-describedby="gol_darah" placeholder="" name="kota">
				</div>

				<div class="form-group col-3">
					<label for="gol_darah">Provinsi</label>
						<input type="text" class="form-control" value="<?= $p->provinsi?>" id="gol_darah" aria-describedby="gol_darah" placeholder="" name="provinsi">
				</div>

				<div class="form-group col-3">
					<label for="pekerjaan">Pengirim</label>
						<input type="text" class="form-control" id="pekerjaan" readonly="" value="<?= $p->nama_lengkap?>" aria-describedby="pekerjaan" placeholder="" name="nama_pengirim">
				</div>

				<div class="form-group col-3">
						<input type="hidden" class="form-control" id="pekerjaan" value="<?php echo $_SESSION['id_user']?>" aria-describedby="pekerjaan" placeholder="" name="id_pengirim">
				</div>

                </div>
                
                <input type="hidden" class="form-control" id="pekerjaan" value="<?= $p->statuspost;?>" aria-describedby="pekerjaan" placeholder="" name="statuspost">
				
				<div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button type="submit" class="btn btn-info">Setuju</button>
                </div>
				</div>
				</div>
			<?php endforeach;?>
</div>