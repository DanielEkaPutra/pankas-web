<!-- Modal -->

<div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="formEditModal">Edit Jemaat</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <form action="<?= site_url('jemaat/update_data');?>" method="post">
		<div class="row">
				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-3">
					<label for="IDJemaat">No ID</label>
						<input type="number" readonly="" class="form-control" id="id_jemaat" name="id_jemaat">
				</div>
				<div class="form-group col-3">
					<label for="Nama">Nama</label>
						<input type="text" class="form-control" id="nama"  name="nama">
				</div>
				
				<div class="form-group col-3">
					<label for="Email">Email</label>
						<input type="email" class="form-control"  id="email" name="email">
				</div>

				<div class="form-group col-3">
					<label for="JenisKelamin">Jenis Kelamin</label>
						<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
						<option value="LAKI LAKI">Laki-Laki</option>
							<option value="PEREMPUAN">Perempuan</option>
						</select>
				</div>
				
				</div>
				</div>
				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-4">
					<label for="Tempat_Lahir">Tempat Lahir</label>
						<input type="text" class="form-control"  id="tempat_lahir" name="tempat_lahir">
				</div>
				
				<div class="form-group col-4">
					<label for="Tempat_Baptis">Tempat Baptis</label>
						<input type="text" class="form-control" id="tempat_baptis" name="tempat_baptis">
				</div>

				<div class="form-group col-4">
					<label for="Tempat_Sidi">Tempat Sidi</label>
						<input type="text" class="form-control" id="tempat_sidi" name="tempat_sidi">
				</div>
				
				</div>
				</div>
				
				<div class="col-xl-12">
				<div class="row">
				
				<div class="form-group col-4">
				<label for="tgl_lahir">Tanggal Lahir</label>
						<input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir">
				</div>

				<div class="form-group col-4">
					<label for="tgl_baptis">Tanggal Baptis</label>
						<input type="date" class="form-control" value="" id="tgl_baptis" name="tgl_baptis">
				</div>
				<div class="form-group col-4">
					<label for="tgl_sidi">Tanggal Sidi</label>
						<input type="date" class="form-control" id="tgl_sidi" name="tgl_sidi">
				</div>
				
                
				</div>
				</div>

				<div class="col-xl-12">
				<div class="row">
				
				<div class="form-group col-4">
					<label for="tempat_nikah">Tempat Nikah</label>
						<input type="text" class="form-control" id="tempat_nikah"  name="tempat_nikah">
				</div>

				<div class="form-group col-4">
					<label for="tempat_pindah">Tempat Pindah</label>
						<input type="text" class="form-control" id="tempat_pindah" name="tempat_pindah">
				</div>
				<div class="form-group col-4">
					<label for="tempat_meninggal">Tempat Meninggal</label>
						<input type="text" class="form-control" id="tempat_meninggal"  name="tempat_meninggal">
				</div>
				
				</div>
				</div>

				<div class="col-xl-12">
				<div class="row">
				
				<div class="form-group col-4">
					<label for="tgl_nikah">Tanggal Nikah</label>
						<input type="date" class="form-control" id="tgl_nikah"  name="tgl_nikah">
				</div>

				<div class="form-group col-4">
					<label for="tgl_pindah">Tanggal Pindah</label>
						<input type="date" class="form-control" id="tgl_pindah" name="tgl_pindah">
				</div>
				<div class="form-group col-4">
					<label for="tgl_meninggal">Tanggal Meninggal</label>
						<input type="date" class="form-control" id="tgl_meninggal" name="tgl_meninggal">
				</div>
				
                
				</div>
				</div>

				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-3">
					<label for="pendidikan">Pendidikan</label>
						<input type="text" class="form-control" id="pendidikan" name="pendidikan">
				</div>
				<div class="form-group col-3">
					<label for="IDSektor">Sektor</label>
					<select name="sektor" id="sektor" class="form-control">
					<option value="0">Pilih Salah Satu</option>
						<option value="1">Yerusalem</option>
						<option  value="2">Betel</option>
						<option  value="3">Sion</option>
						<option  value="4">Hermon</option>
						<option  value="5">Yerikho</option>
						<option  value="6">Karmel</option>
						<option  value="7">Pniel</option>
						<option  value="8">Nazaret</option>
						<option  value="9">Moria</option>
						<option   value="10">Getsemani</option>
						<option   value="11">Betesda</option>
						<option   value="12">Betlehem</option>
						<option   value="13">Betesda</option>
					</select>
				</div>
				
				<div class="form-group col-3">
					<label for="Telepon">Telepon</label>
						<input type="number" class="form-control" id="telepon" name="telepon">
				</div>
				
				<div class="form-group col-3">
					<label for="pekerjaan">Pekerjaan</label>
						<input type="text" class="form-control" id="pekerjaan" name="pekerjaan">
				</div>

				</div>
				</div>

				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-3">
					<label for="alamat">Alamat</label>
						<input type="text" class="form-control" id="alamat" name="alamat">
				</div>				
				
				<div class="form-group col-3">
					<label for="Kota">Kota</label>
						<input type="text" class="form-control" id="Kota" name="kota">
				</div>

				<div class="form-group col-3">
					<label for="provinsi">Provinsi</label>
						<input type="text" class="form-control" id="provinsi"  name="provinsi">
				</div>

				<div class="form-group col-3">
					<label for="pekerjaan">Pengirim</label>
						<input type="text" class="form-control" id="pekerjaan" readonly="" value="<?php echo $_SESSION['nama_lengkap']?>" aria-describedby="pekerjaan" placeholder="" name="nama_pengirim">
				</div>

				<div class="form-group col-3">
						<input type="hidden" class="form-control" id="pekerjaan" value="<?php echo $_SESSION['id_user']?>" aria-describedby="pekerjaan" placeholder="" name="id_pengirim">
				</div>

				</div>
				
				<div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button type="submit" class="btn btn-info">Update</button>
                </div>
				</div>
				</div>
</div>