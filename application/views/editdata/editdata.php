
<?php foreach($jemaat as $j)?>
<form action ="<?= site_url('jemaat/update_data'); ?>" method="post">
	<div class="row">
			<!-- Area Chart -->
			<div class="col-xl-12 col-lg-7">
			  <div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<!-- Card Body -->
				
				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-4">
					<label for="no_kartu_keluarga">No Kartu Keluarga</label>
						<input type="number" value="<?= $j->no_kk?>" class="form-control" id="no_kartu_keluarga" aria-describedby="no_kartu_keluarga" placeholder="" name="no_kk">
				</div>
				<div class="form-group col-4">
					<label for="IDJemaat">No ID Jemaat</label>
						<input type="number" value="<?= $j->id_jemaat?>"class="form-control" id="IDJemaat" aria-describedby="id_jemaat" placeholder="" name="id_jemaat">
				</div>
				<div class="form-group col-4">
					<label for="Nama">Nama</label>
						<input type="text" value="<?= $j->nama?>" class="form-control" id="Nama" aria-describedby="Nama" placeholder="" name="nama">
				</div>
				</div>
				</div>
				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-4">
					<label for="Alamat">Alamat</label>
						<input type="text" value="<?= $j->alamat?>" class="form-control" id="Alamat" aria-describedby="Alamat" placeholder="" name="alamat">
				</div>
				<div class="form-group col-4">
					<label for="Tempat_Lahir">Tempat Lahir</label>
						<input type="text" value="<?= $j->tempat_lahir?>" class="form-control" id="Tempat_Lahir" aria-describedby="Tempat_Lahir" placeholder="" name="tempat_lahir">
				</div>
				
				<div class="form-group col-4">
					<label for="tgl_lahir">Tanggal Lahir</label>
						<input type="date" value="<?= $j->tgl_lahir?>" class="form-control" id="tgl_lahir" aria-describedby="tgl_lahir" placeholder="" name="tgl_lahir">
				</div>
				</div>
				</div>
				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-3">
					<label for="tgl_baptis">Tanggal Baptis</label>
						<input type="date" value="<?= $j->tgl_baptis?>" class="form-control" id="tgl_baptis" aria-describedby="tgl_baptis" placeholder="" name="tgl_baptis">
				</div>
				<div class="form-group col-3">
					<label for="tgl_sidi">Tanggal Sidi</label>
						<input type="date" value="<?= $j->tgl_sidi?>" class="form-control" id="tgl_sidi" aria-describedby="tgl_sidi" placeholder="" name="tgl_sidi">
				</div>
				<div class="form-group col-3">
					<label for="tgl_nikah">Tanggal Nikah</label>
						<input type="date" value="<?= $j->tgl_nikah?>" class="form-control" id="tgl_nikah" aria-describedby="tgl_nikah" placeholder="" name="tgl_nikah">
				</div>
                
				</div>
				</div>
				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-4">
					<label for="pendidikan">Pendidikan</label>
						<input type="text" value="<?= $j->pendidikan?>" class="form-control" id="pendidikan" aria-describedby="pendidikan" placeholder="" name="pendidikan">
				</div>
				<div class="form-group col-4">
					<label for="IDSektor">Sektor</label>
					<select name="sektor" id="sektor" class="form-control">
						<option value="0">Pilih Salah Satu</option>
						<option <?php if($j->sektor == "1"){echo "selected='selected'";} echo $j->sektor;?> value="1">Yerusalem</option>
						<option <?php if($j->sektor == "2"){echo "selected='selected'";} echo $j->sektor;?> value="2">Betel</option>
						<option <?php if($j->sektor == "3"){echo "selected='selected'";} echo $j->sektor;?> value="3">Sion</option>
						<option <?php if($j->sektor == "4"){echo "selected='selected'";} echo $j->sektor;?> value="4">Hermon</option>
						<option <?php if($j->sektor == "5"){echo "selected='selected'";} echo $j->sektor;?> value="5">Yerikho</option>
						<option <?php if($j->sektor == "6"){echo "selected='selected'";} echo $j->sektor;?> value="6">Karmel</option>
						<option <?php if($j->sektor == "7"){echo "selected='selected'";} echo $j->sektor;?> value="7">Pniel</option>
						<option <?php if($j->sektor == "8"){echo "selected='selected'";} echo $j->sektor;?> value="8">Nazaret</option>
						<option <?php if($j->sektor == "9"){echo "selected='selected'";} echo $j->sektor;?> value="9">Moria</option>
						<option <?php if($j->sektor == "10"){echo "selected='selected'";} echo $j->sektor;?> value="10">Getsemani</option>
						<option <?php if($j->sektor == "11"){echo "selected='selected'";} echo $j->sektor;?> value="11">Betesda</option>
						<option <?php if($j->sektor == "12"){echo "selected='selected'";} echo $j->sektor;?> value="12">Betlehem</option>
						<option <?php if($j->sektor == "13"){echo "selected='selected'";} echo $j->sektor;?> value="13">Betesda</option>
					</select>
				</div>
				
				<div class="form-group col-4">
					<label for="JenisKelamin">Jenis Kelamin</label>
						<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
							<option <?php if($j->jenis_kelamin == "Laki-Laki"){echo "selected='selected'";} echo $j->jenis_kelamin;?> value="Laki-Laki">Laki-Laki</option>
							<option <?php if($j->jenis_kelamin == "Perempuan"){echo "selected='selected'";} echo $j->jenis_kelamin;?>value="Perempuan">Perempuan</option>
						</select>
				</div>
				</div>
				</div>

				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-3">
					<label for="hub_keluarga">Hubungan Keluarga</label>
						<input type="text" value="<?= $j->hub_keluarga?>" class="form-control" id="hub_keluarga" aria-describedby="hub_keluarga" placeholder="" name="hub_keluarga">
				</div>
				<div class="form-group col-3">
					<label for="pekerjaan">Pekerjaan</label>
						<input type="text" value="<?= $j->pekerjaan?>" class="form-control" id="pekerjaan" aria-describedby="pekerjaan" placeholder="" name="pekerjaan">
				</div>
				
				<div class="form-group col-3">
					<label for="Telepon">No HP</label>
						<input type="number" value="<?= $j->no_hp?>" class="form-control" id="Telepon" aria-describedby="Telepon" placeholder="" name="no_hp">
				</div>
				
				<div class="form-group col-3">
					<label for="gol_darah">Golongan Darah</label>
						<input type="text" value="<?= $j->gol_darah?>" class="form-control" id="gol_darah" aria-describedby="gol_darah" placeholder="" name="gol_darah">
				</div>
				</div>
				
				<button type="submit" class="btn btn-primary">Submit</button>
				</div>
				</div>
				</div>
				</div>
</form>

