<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin 2 - Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url();?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  
  <!-- Custom styles for this template-->
  <link href="<?= base_url();?>assets/css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Datatable -->
    <!-- <link href="<?= base_url();?>assets/dataTables/datatables.min.css" rel="stylesheet"> -->
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php
      $this->load->view('template_dashboard/sidenav');
    ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php
          $this->load->view('template_dashboard/nav');
        ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <?php
            $this->load->view('template_dashboard/header');
          ?>

          <?php
            $this->load->view('lihatdata/lihatdata');
          ?>

          

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <?php
         $this->load->view('template_dashboard/footer');
      ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <?php
    $this->load->view('template_dashboard/modal');
  ?>

  <?php
    $this->load->view('template_dashboard/modal_edit');
  ?>

  <!-- Bootstrap core JavaScript-->

  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" 
integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>

    <script src="<?= base_url(); ?>assets/vendor/bootstrap/js/bootstrap.js"></script>

  <!-- Core plugin JavaScript-->
 <!-- <script src="<?= base_url(); ?>assets/vendor/jquery-easing/jquery.easing.min.js"></script> -->

  <!-- Custom scripts for all pages-->
  <!-- <script src="<?= base_url(); ?>assets/js/sb-admin-2.min.js"></script> -->

  <!-- Page level plugins -->
  <!-- Page level custom scripts -->

  <!-- <script src="<<?= base_url(); ?>assets/dataTables/datatables.min.js"></script> -->
  <script src="<?= base_url(); ?>assets/js/script.js"></script>
</body>
</html>
