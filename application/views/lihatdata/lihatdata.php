<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Data Jemaat</h6>
            </div>
            <div class="card-body">
            <div class="row">
              <div class="col-md-4">
        
              <form action="<?= base_url('jemaat');?>" method="post">
                <div class="input-group mb-1">
                <input type="text" class="form-control" placeholder="Search Keywords .." name="keyword"
                autocomplete="off" autofocus>
                  <div class="input-group-append">
                  <input class="btn btn-primary" type="submit" name="submit" value="Cari">
                   </div>
                </div>
                </form>
              </div>
            </div> 
            <h5> Results: <?= $total_rows; ?></h5>
            <?= $this->pagination->create_links(); ?>
            <div class="table-responsive">
              <table class="table table-striped table-bordered" id="table" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                          <th>No</th>
                          <th>ID Jemaat</th>
                          <th>Nama</th>
                          <th>Alamat</th>
                          <th>Sektor</th>
                          <th>Jenis Kelamin</th>
                          <th>Pilihan</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php if(empty($jemaat)) : ?>
                    <tr>
                      <td colspan = "20">
                      <div class="alert alert-danger" role="alert">
                        data not found!
                      </div>
                    </td>
                    </tr>
                    
                      <?php endif; ?>
                  
                  
                  
                  <?php
                        foreach($jemaat as $j) :
                    ?>
                    <tr>
                          <td><?php echo ++$start?></td>
                          <td><?= $j->id_jemaat; ?></td>
                          <td><?= $j->nama; ?></td>
                          <td><?= $j->alamat; ?></td>
                          <td><?= $j->sektor; ?></td>
                          <td><?= $j->jenis_kelamin; ?></td>
                          <td>
                              
                              <button class="btn btn-success btn-sm edit_data" data-toggle="modal" data-target="#editModal" data-id="<?= $j->id_jemaat; ?>"> <i class="fa fa-edit"></i> Edit </button> </a>
                              <a href="<?= base_url(). 'jemaat/hapus_data/' .$j->id_jemaat;?>"><button class="btn btn-danger btn-sm hapus_data"> <i class="fa fa-trash"></i> Hapus </button>
                          </td>
                      </tr>
                        <?php endforeach; ?>
                  </tbody>
                </table>
              </div>

            </div>
          </div>

</div>
        <!-- /.container-fluid -->

      </div>

      
      <!-- End of Main Content -->

<!-- <script type="text/javascript">

$(document).ready( function () {
    $('#table').DataTable();
} );

</script> -->