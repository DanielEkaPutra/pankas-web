

<form action ="<?= site_url('jemaat/tambah_data'); ?>" method="post">
	<div class="row">
			<!-- Area Chart -->
			<div class="col-xl-12 col-lg-7">
			  <div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<!-- Card Body -->
				
				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-3">
					<label for="IDJemaat">No ID Jemaat</label>
						<input type="number" class="form-control" id="IDJemaat" aria-describedby="id_jemaat" placeholder="" name="id_jemaat">
				</div>
				<div class="form-group col-3">
					<label for="Nama">Nama</label>
						<input type="text" class="form-control" id="Nama" aria-describedby="Nama" placeholder="" name="nama">
				</div>
				
				<div class="form-group col-3">
					<label for="Email">Email</label>
						<input type="email" class="form-control" id="Alamat" aria-describedby="Alamat" placeholder="" name="email">
				</div>

				<div class="form-group col-3">
					<label for="JenisKelamin">Jenis Kelamin</label>
						<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
							<option value="Laki-Laki">Laki-Laki</option>
							<option value="Perempuan">Perempuan</option>
						</select>
				</div>
				
				</div>
				</div>
				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-4">
					<label for="Tempat_Lahir">Tempat Lahir</label>
						<input type="text" class="form-control" id="Tempat_Lahir" aria-describedby="Tempat_Lahir" placeholder="" name="tempat_lahir">
				</div>
				
				<div class="form-group col-4">
					<label for="Tempat_Baptis">Tempat Baptis</label>
						<input type="text" class="form-control" id="Tempat_Lahir" aria-describedby="Tempat_Lahir" placeholder="" name="tempat_baptis">
				</div>

				<div class="form-group col-4">
					<label for="Tempat_Sidi">Tempat Sidi</label>
						<input type="text" class="form-control" id="Tempat_Lahir" aria-describedby="Tempat_Lahir" placeholder="" name="tempat_sidi">
				</div>
				
				</div>
				</div>
				
				<div class="col-xl-12">
				<div class="row">
				
				<div class="form-group col-4">
				<label for="tgl_lahir">Tanggal Lahir</label>
						<input type="date" class="form-control" id="tgl_lahir" aria-describedby="tgl_lahir" placeholder="" name="tgl_lahir">
				</div>

				<div class="form-group col-4">
					<label for="tgl_lahir">Tanggal Baptis</label>
						<input type="date" class="form-control" id="tgl_lahir" aria-describedby="tgl_lahir" placeholder="" name="tgl_baptis">
				</div>
				<div class="form-group col-4">
					<label for="tgl_sidi">Tanggal Sidi</label>
						<input type="date" class="form-control" id="tgl_sidi" aria-describedby="tgl_sidi" placeholder="" name="tgl_sidi">
				</div>
				
                
				</div>
				</div>

				<div class="col-xl-12">
				<div class="row">
				
				<div class="form-group col-4">
					<label for="Tempat_Lahir">Tempat Nikah</label>
						<input type="text" class="form-control" id="Tempat_Lahir" aria-describedby="Tempat_Lahir" placeholder="" name="tempat_nikah">
				</div>

				<div class="form-group col-4">
					<label for="Tempat_Lahir">Tempat Pindah</label>
						<input type="text" class="form-control" id="Tempat_Lahir" aria-describedby="Tempat_Lahir" placeholder="" name="tempat_pindah">
				</div>
				<div class="form-group col-4">
					<label for="Tempat_Lahir">Tempat Meninggal</label>
						<input type="text" class="form-control" id="Tempat_Lahir" aria-describedby="Tempat_Lahir" placeholder="" name="tempat_meninggal">
				</div>
				
                
				</div>
				</div>

				<div class="col-xl-12">
				<div class="row">
				
				<div class="form-group col-4">
					<label for="tgl_lahir">Tanggal Nikah</label>
						<input type="date" class="form-control" id="tgl_lahir" aria-describedby="tgl_lahir" placeholder="" name="tgl_nikah">
				</div>

				<div class="form-group col-4">
					<label for="tgl_lahir">Tanggal Pindah</label>
						<input type="date" class="form-control" id="tgl_lahir" aria-describedby="tgl_lahir" placeholder="" name="tgl_pindah">
				</div>
				<div class="form-group col-4">
					<label for="tgl_sidi">Tanggal Meninggal</label>
						<input type="date" class="form-control" id="tgl_sidi" aria-describedby="tgl_sidi" placeholder="" name="tgl_meninggal">
				</div>
				
                
				</div>
				</div>

				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-3">
					<label for="pendidikan">Pendidikan</label>
						<input type="text" class="form-control" id="pendidikan" aria-describedby="pendidikan" placeholder="" name="pendidikan">
				</div>
				<div class="form-group col-3">
					<label for="IDSektor">Sektor</label>
					<select name="sektor" id="sektor" class="form-control">
						<option value="0">Pilih Salah Satu</option>
						<option value="1">Yerusalem</option>
						<option value="2">Betel</option>
						<option value="3">Sion</option>
						<option value="4">Hermon</option>
						<option value="5">Yerikho</option>
						<option value="6">Karmel</option>
						<option value="7">Pniel</option>
						<option value="8">Nazaret</option>
						<option value="9">Moria</option>
						<option value="10">Getsemani</option>
						<option value="11">Betesda</option>
						<option value="12">Betlehem</option>
						<option value="13">Betesda</option>
					</select>
				</div>
				
				<div class="form-group col-3">
					<label for="Telepon">Telepon</label>
						<input type="number" class="form-control" id="Telepon" aria-describedby="Telepon" placeholder="" name="telepon">
				</div>
				
				<div class="form-group col-3">
					<label for="pekerjaan">Pekerjaan</label>
						<input type="text" class="form-control" id="pekerjaan" aria-describedby="pekerjaan" placeholder="" name="pekerjaan">
				</div>

				</div>
				</div>

				<div class="col-xl-12">
				<div class="row">
				<div class="form-group col-3">
					<label for="hub_keluarga">Alamat</label>
						<input type="text" class="form-control" id="hub_keluarga" aria-describedby="hub_keluarga" placeholder="" name="alamat">
				</div>				
				
				<div class="form-group col-3">
					<label for="gol_darah">Kota</label>
						<input type="text" class="form-control" id="gol_darah" aria-describedby="gol_darah" placeholder="" name="kota">
				</div>

				<div class="form-group col-3">
					<label for="gol_darah">Provinsi</label>
						<input type="text" class="form-control" id="gol_darah" aria-describedby="gol_darah" placeholder="" name="provinsi">
				</div>

				<div class="form-group col-3">
					<label for="pekerjaan">Pengirim</label>
						<input type="text" class="form-control" id="pekerjaan" readonly="" value="<?php echo $_SESSION['nama_lengkap']?>" aria-describedby="pekerjaan" placeholder="" name="nama_pengirim">
				</div>

				<div class="form-group col-3">
						<input type="hidden" class="form-control" id="pekerjaan" value="<?php echo $_SESSION['id_user']?>" aria-describedby="pekerjaan" placeholder="" name="id_pengirim">
				</div>

				</div>
				
				<button type="submit" class="btn btn-primary">Submit</button>
				</div>
				</div>
				</div>
				</div>
</form>

